// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  compatibilityDate: '2024-11-01',
  devtools: { enabled: true },
  modules: [
    '@nuxt/ui',
    'nuxt-umami',
    '@nuxt/eslint',
    '@nuxtjs/i18n',
    '@nuxtjs/color-mode'
  ],
  umami: {
    enabled: !!process.env.UM_ENABLED,
    id: process.env.UM_ID,
    host: process.env.UM_HOST,
    customEndpoint: process.env.UM_ENDPOINT,
    autoTrack: true,
    domains: process.env.UM_DOMAINS?.toString().split(','),
    tag: `Env-${process.env.NODE_ENV}`
  },
  colorMode: {
    preference: 'system',
    fallback: 'dark',
    storage: 'localStorage'
  },
  i18n: {
    locales: ['en', 'de'],
    defaultLocale: 'en',
    vueI18n: '~/i18n.config.ts'
  }
})