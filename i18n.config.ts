export default defineI18nConfig(() => ({
  legacy: false,
  locale: 'en',
  messages: {
    en: {
      general_test: 'This is a test message',
      menu_home: 'Home',
      menu_about: 'About',
      menu_projects: 'Projects',
      menu_contact: 'Contact',
    },
    de: {
      general_test: 'Das ist eine Testnachricht',
      menu_home: 'Home',
      menu_about: 'About',
      menu_projects: 'Projekte',
      menu_contact: 'Kontakt',
    }
  }
}))